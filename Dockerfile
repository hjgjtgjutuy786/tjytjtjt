FROM ubuntu:20.04

RUN apt-get update && \
    apt-get install -y screen wget git curl sudo && \
    sudo useradd -m hoki && \
    sudo adduser hoki sudo && \
    sudo usermod -a -G sudo hoki && \
    sudo echo 'hoki:hoki' | sudo chpasswd
    
ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

CMD /entrypoint.sh